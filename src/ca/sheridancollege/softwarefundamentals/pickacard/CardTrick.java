/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 *
 * @author Zalak
 */

import java.util.Scanner;

public class CardTrick
{

   public static void main (String[] args)
   {

      Scanner in = new Scanner(System.in);
      Card c = new Card();
      Card[] magicHand = new Card[7];

      for (int i = 0; i < magicHand.length; i++) {
         //   Card c = new Card();

         c.setValue(c.randomValue());//c.setValue(insert call to random number generator here)
         c.setSuit(Card.SUITS[c.randomSuit()]); //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
         magicHand[i] = c;
      }

      //insert code to ask the user for Card value and suit, create their card
      // and search magicHand here
      //Then report the result here
      for (Card magic : magicHand) {
         System.out.println(magic.getSuit() + " " + magic.getValue());
      }
      System.out.println("Pick a card");
      System.out.println("Enter the card value");
      int UserValue = in.nextInt();

      System.out.println("Enter the card suit");
      System.out.println("1 = Hearts, 2 = Diamonds, 3 = Spades, 4 = Clubs");
      int UserSuit = in.nextInt();

      System.out.println("Your card is: " + UserValue + " of " + UserSuit);
      System.out.println("Checking if your card is in magic ha         count = 1;\n"
          + "nd");

      if (UserValue == magic.getValue() && UserSuit == magic()) {
         System.out.println("Wow!! You did a great job");

      }
   }
}
